package com.twuc.webApp.bean;

public class IllegalArgumentExBean {

    private String message;

    public IllegalArgumentExBean() {
    }

    public IllegalArgumentExBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "IllegalArgumentExBean{" +
                "message='" + message + '\'' +
                '}';
    }
}
