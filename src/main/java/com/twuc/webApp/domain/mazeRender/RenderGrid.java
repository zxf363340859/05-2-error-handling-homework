package com.twuc.webApp.domain.mazeRender;

import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeGenerator.GridCell;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class RenderGrid {
    private int rowCount;
    private int columnCount;
    private final RenderCell[][] renderGrid;

    public RenderGrid(Grid grid) {
        rowCount = translateGridIndex(grid.getRowCount());
        columnCount = translateGridIndex(grid.getColumnCount());
        renderGrid = allocateGrid();
        prepareGrid(grid);
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public List<RenderCell> getCells() {
        return Arrays.stream(renderGrid).flatMap(Arrays::stream).collect(Collectors.toList());
    }

    private void prepareGrid(Grid grid) {
        configureNeighbors();
        configureCellTypes(grid);
    }

    private void configureCellTypes(Grid grid) {
        setWesternWall(renderGrid);
        setNorthenWall(renderGrid);
        setCellAndLinkedPart(renderGrid, grid);
        removeIsolatedConnector(renderGrid);
        setDirection(renderGrid);
    }

    private void removeIsolatedConnector(RenderCell[][] renderGrid) {
        for (int rowIndex = 2; rowIndex < rowCount - 1; rowIndex += 2) {
            for (int columnIndex = 2; columnIndex < columnCount - 1; columnIndex += 2) {
                RenderCell connector = renderGrid[rowIndex][columnIndex];
                if (connector.getNeighbors().stream().allMatch(n -> n.getRenderType() != RenderType.WALL)) {
                    connector.setRenderType(RenderType.GROUND);
                }
            }
        }
    }

    private void configureNeighbors() {
        for (RenderCell[] rows : renderGrid) {
            for (RenderCell cell : rows) {
                int cellRow = cell.getRow();
                int cellColumn = cell.getColumn();
                cell.setNeighbors(
                    this.getCell(cellRow - 1, cellColumn),
                    this.getCell(cellRow + 1, cellColumn),
                    this.getCell(cellRow, cellColumn + 1),
                    this.getCell(cellRow, cellColumn - 1));
            }
        }
    }

    public RenderCell getCell(int rowIndex, int columnIndex) {
        if (rowIndex < 0 || rowIndex >= rowCount) return null;
        if (columnIndex < 0 || columnIndex >= columnCount) return null;
        return renderGrid[rowIndex][columnIndex];
    }

    private RenderCell[][] allocateGrid() {
        RenderCell[][] theGrid = new RenderCell[rowCount][];
        for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
            RenderCell[] currentRow = new RenderCell[columnCount];
            for (int columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
                currentRow[columnIndex] = new RenderCell(rowIndex, columnIndex);
            }

            theGrid[rowIndex] = currentRow;
        }

        return theGrid;
    }

    private void setWesternWall(RenderCell[][] renderGrid) {
        for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
            renderGrid[rowIndex][0].setRenderType(RenderType.WALL);
        }
    }

    private void setNorthenWall(RenderCell[][] renderGrid) {
        for (int columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
            renderGrid[0][columnIndex].setRenderType(RenderType.WALL);
        }
    }

    private static void setCellAndLinkedPart(RenderCell[][] renderGrid, Grid grid) {
        for (GridCell cell : grid.getCells()) {
            int centerRowIndex = translateGridIndex(cell.getRow());
            int centerColumnIndex = translateGridIndex(cell.getColumn());
            RenderCell center = renderGrid[centerRowIndex][centerColumnIndex];
            center.setRenderType(RenderType.GROUND);
            center.setTags(cell.getTags());
            renderGrid[centerRowIndex + 1][centerColumnIndex + 1].setRenderType(RenderType.WALL);
            renderGrid[centerRowIndex][centerColumnIndex + 1].setRenderType(
                cell.isLinked(cell.getEast()) ? RenderType.GROUND : RenderType.WALL);
            renderGrid[centerRowIndex + 1][centerColumnIndex].setRenderType(
                cell.isLinked(cell.getSouth()) ? RenderType.GROUND : RenderType.WALL);
        }
    }

    private static void setDirection(RenderCell[][] renderGrid) {
        for (RenderCell[] rows : renderGrid) {
            for (RenderCell cell : rows) {
                RenderType selfType = cell.getRenderType();
                if (cell.getEast() != null && cell.getEast().getRenderType() == selfType) {
                    cell.setDirection(cell.getDirection() | Direction.EAST);
                }
                if (cell.getWest() != null && cell.getWest().getRenderType() == selfType) {
                    cell.setDirection(cell.getDirection() | Direction.WEST);
                }
                if (cell.getNorth() != null && cell.getNorth().getRenderType() == selfType) {
                    cell.setDirection(cell.getDirection() | Direction.NORTH);
                }
                if (cell.getSouth() != null && cell.getSouth().getRenderType() == selfType) {
                    cell.setDirection(cell.getDirection() | Direction.SOUTH);
                }
            }
        }
    }

    private static int translateGridIndex(int index) {
        return 1 + index * 2;
    }
}
