package com.twuc.webApp.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.bean.IllegalArgumentExBean;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MazeExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger logger;

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handlerException(IllegalArgumentException illegalArgumentException) throws JsonProcessingException {
        IllegalArgumentExBean illegalArgumentExBean = new IllegalArgumentExBean(illegalArgumentException.getMessage());
        ObjectMapper objectMapper = new ObjectMapper();
        String bodyConent = objectMapper.writeValueAsString(illegalArgumentExBean);
        System.out.println(bodyConent);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON_UTF8).body(bodyConent);
    }




}
