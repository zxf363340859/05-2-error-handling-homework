package com.twuc.webApp.web.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void shoule_return_status_400_when_type_error() throws Exception {
        mockMvc.perform(get("/mazes/asdas").param("width", "11").param("height", "11"))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string("{\"message\":\"Invalid type: asdas\"}"));
    }

    @Test
    void shoule_return_status_400_when_width_error() throws Exception {
        mockMvc.perform(get("/mazes/UNKNOWN").param("width", "1").param("height", "1"))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string("{\"message\":\"The width(1) is not valid\"}"));
    }

    @Test
    void shoule_return_status_400_() throws Exception {
        mockMvc.perform(get("/mazes/UNKNOWN").param("width", "11").param("height", "1"))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string("{\"message\":\"The height(1) is not valid\"}"));
    }
}
